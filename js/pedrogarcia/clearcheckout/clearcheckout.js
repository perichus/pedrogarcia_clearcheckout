document.observe("dom:loaded", function() {
    if ($('register-customer-password')) {
        Element.hide('register-customer-password');
    }
});

Checkout.addMethods({
    updatePayment: function()
    {
        $$('[name="shipping_method"]').each(function(element) {
            element.observe('click', function(){
                shippingMethod.save();
                checkout.reloadProgressBlock();
            });
        });
    },

    updateTopProgress: function()
    {
        var currentSection = this.accordion.currentSection;
        $$('.clearcheckout__progress li').each(function(e) {
            e.removeClassName('current');
        });
        $$('.clearcheckout__progress li.progress-'+currentSection)[0].addClassName('current allow');

    },

    gotoSection: function(section)
    {
        var currentSection = section;
        section = $('opc-'+section);
        section.previous().addClassName('previous');
        section.addClassName('current');
        Effect.ScrollTo('checkoutSteps', { duration:'0.4', offset:-20 });
        if (currentSection === 'shipping_method' || currentSection === 'payment') {
            this.updatePayment();
            shippingMethod.silentSave();
            $('opc-billing').addClassName('previous');
            if ($$('.sp-methods input:checked[name=shipping_method]').length == 1) {
                shippingMethod.save();
                checkout.reloadProgressBlock();
                if ($$('.sp-methods input:checked[name=payment[method]]').length == 1) {
                    payment.save();
                    checkout.reloadProgressBlock();
                }
            }
            if ($$('.sp-methods .no-display input:checked[type=radio]')) {
                checkout.reloadProgressBlock();
            }
        } else if (currentSection == 'review') {
            $('opc-billing').addClassName('previous');
            $('opc-shipping').addClassName('previous');
        } else if (currentSection == 'shipping') {
            $('opc-billing').addClassName('previous');
        } else {
            $$('.clearcheckout__step').each(function(e) {
                e.removeClassName('previous');
                e.removeClassName('current');
            });
        }
        this.accordion.openSection(section);
        this.reloadProgressBlock();
        this.updateTopProgress();
    },

    gotoSectionFromTop: function(section)
    {
        if ($$('.clearcheckout__progress li.progress-opc-'+section)[0].hasClassName('allow')) {
            this.gotoSection(section);
        }
    },

    back: function()
    {
        if (this.loadWaiting) return;
        var section = $(this.accordion.currentSection);
        section.previous().removeClassName('previous');
        if (section.hasClassName('delivery-payment')) {
            $('opc-billing').addClassName('current').removeClassName('previous');
        }
    },

    showPassword: function()
    {
        if ($('register_new_account') && $('register_new_account').checked) {
            this.method = 'register';
            var request = new Ajax.Request(
                this.saveMethodUrl,
                {method: 'post', onFailure: this.ajaxFailure.bind(this), parameters: {method:'register'}}
            );
            Element.show('register-customer-password');
            this.gotoSection('billing');
        }
        else {
            this.method = 'guest';
            var request = new Ajax.Request(
                this.saveMethodUrl,
                {method: 'post', onFailure: this.ajaxFailure.bind(this), parameters: {method:'guest'}}
            );
            Element.hide('register-customer-password');
            this.gotoSection('billing');
        }
    },

    clearCheckout: function()
    {
        this.method = 'guest';
        var request = new Ajax.Request(
            this.saveMethodUrl,
            {method: 'post', onFailure: this.ajaxFailure.bind(this), parameters: {method:'guest'}}
        );
        Element.hide('register-customer-password');
        this.gotoSection('billing');

        var loginLink = '<a href="#" class="register-link">'+ Translator.translate("Login") +'</a>';
        var passwordInput = '<li class="control"><input type="checkbox" name="register_new_account" value="0" title="" id="register_new_account" onclick="checkout.showPassword()" class="checkbox"><label for="register_new_account" class="checkboxlabel">'+ Translator.translate("I want to register with this data") +'</label></li>';
        var popupBg = '<div id="popup-bg" class="popup__bg"></div>';

        if ($('register-customer-password')) {
            $$('.clearcheckout__progress-list')[0].insert({before: loginLink});
            $$('#co-billing-form .form-list')[0].insert(passwordInput);
            var closePopup = function() {
                $('popup-bg').observe('click', function(){
                    $('popup-bg').remove();
                    $$('.address__login')[0].toggleClassName('is-visible');
                });
            };

            $$('.register-link')[0].observe('click', function(e){
                Event.stop(e);
                $$('body')[0].insert(popupBg);
                closePopup();
                $$('.address__login')[0].toggleClassName('is-visible');
            });

            if ($$('.address__login .messages')[0] != undefined) {
                $$('body')[0].insert(popupBg);
                closePopup();
                $$('.address__login')[0].toggleClassName('is-visible');
            }

        }


        $$('[name="billing[use_for_shipping]"]').each(function(element) {
            element.observe('click', function(){
                if (($('billing:use_for_shipping_yes')) && ($('billing:use_for_shipping_yes').checked)) {
                    shipping.syncWithBilling();
                    $('opc-billing').removeClassName('previous');

                } else if (($('billing:use_for_shipping_no')) && ($('billing:use_for_shipping_no').checked)) {
                    $('shipping:same_as_billing').checked = false;
                    var validator = new Validation(this.form);
                    if (validator.validate()) {
                        billing.save();
                        checkout.gotoSection('shipping');
                        $('opc-billing').addClassName('previous');
                    }
                }
            });
        });
    }
});

ShippingMethod.addMethods({
    silentSave: function(){
        if (this.validate()) {
            var request = new Ajax.Request(
                this.saveUrl,
                {
                    method:'post',
                    onComplete: this.onComplete,
                    onSuccess: this.onSave,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: Form.serialize(this.form)
                }
            );
        }
    }
});

Payment.addMethods({
    save: function(){
        if (checkout.loadWaiting!=false) return;
        var validator = new Validation(this.form);
        if (this.validate() && validator.validate()) {
            checkout.setLoadWaiting('payment');
            var request = new Ajax.Request(
                this.saveUrl,
                {
                    method:'post',
                    onComplete: this.onComplete,
                    onSuccess: this.onSave,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: Form.serialize(this.form)
                }
            );
        }
    }
});
