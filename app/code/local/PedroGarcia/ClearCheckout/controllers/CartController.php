<?php
/**
 * @author     Pedro García
 * @copyright  Copyright 2014 Pedro García http://pedrogarcia.dev
 */
require_once 'Mage/Checkout/controllers/CartController.php';


class PedroGarcia_ClearCheckout_CartController extends Mage_Checkout_CartController {
    public function couponPostAction() {
        if(!isset($_POST['ajax']))
        {
            parent::couponPostAction();
            return;
        }
        
        $response = array();

        $couponCode = (string) $this->getRequest()->getParam('coupon_code');
        if ($this->getRequest()->getParam('remove') == 1) {
            $couponCode = '';
        }
        $oldCouponCode = $this->_getQuote()->getCouponCode();

        if (!strlen($couponCode) && !strlen($oldCouponCode)) {
            //$this->_goBack();
            $response['message'] = $this->__('Insert coupon code.');
            $response['status'] = 'error';
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
            return;
        }

        try {
            $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
            $this->_getQuote()->setCouponCode(strlen($couponCode) ? $couponCode : '')
                ->collectTotals()
                ->save();

            if ($couponCode) {
                if ($couponCode == $this->_getQuote()->getCouponCode()) {
                    $this->_getSession()->addSuccess(
                        $response['message'] = $this->__('Coupon code "%s" was applied.', Mage::helper('core')->htmlEscape($couponCode))
                    );
                    $response['status'] = 'success';
                }
                else {
                    $response['message'] = $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->htmlEscape($couponCode));
                    $response['status'] = 'error';
                }
            } else {
                $response['status'] = 'cancel';
                $this->_getSession()->addSuccess($response['message'] = $this->__('Coupon code was canceled.'));
            }

        } catch (Mage_Core_Exception $e) {
            $response['message'] = $e->getMessage();
        } catch (Exception $e) {
            $response['message'] = $this->__('Cannot apply the coupon code.');
            Mage::logException($e);
        }

        $this->loadLayout(false);
        $response['totals'] = $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml();

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return;

    }
}